# senior-network
Bilioteca para obter informações de conexão em dispositivos móveis.

## Plugins
* https://github.com/apache/cordova-plugin-network-information


## Como utilizar

### Faça o download da dependência

```
    bower install http://git.senior.com.br/mobilidade/mobilidade-frontend-login.git --save-dev
```

### Adicione a biblioteca no index.html do seu projeto.

```
    <script src="lib/senior-network/network.js"></script>
```

### Adicione os plugins

Se estiver utilizando a biblioteca em um projeto Ionic você pode utilizar os comandos ionic state save/ restore para salvar as configurações de ambiente. Dessa maneira todas as plataformas e plugins do projeto podem ser restaurados com um único comando.

O plugin é necessário apenas para funcionamento em dispositivos móveis. Para instruções de como adicioná-lo consulte a documentação do mesmo em https://github.com/apache/cordova-plugin-network-information.


### Adicione a dependência no módulo

```
    angular.module('starter', ['network'])
```


## Preparando o ambiente de desenvolvimento

### Adicionando dependencias (NPM)

```
    npm install
```

### Adicionando dependências (bower)

```
    bower install
```
