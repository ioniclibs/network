/**
 * @ngdoc overview
 * @name network
 *
 * @description
 * Módulo com funções utilitárias para consutal de status da internet.
 *
 * @requires network.constant
 */
angular.module('network', ['network.constant']);

/**
 * @ngdoc overview
 * @name network.constant
 *
 * @description
 * Constantes utilizadas no módulo network.
 *
 */
angular.module("network.constant", [])

    /**
     * @ngdoc object
     * @name network.constant.NetworkEvents
     * @description
     *
     * - **`NetworkEvents.ON_CONNECTIVITY_CHANGED`** - {string} - Nome de evento para quando há alterações na conectividade.
     */
    .constant("NetworkEvents", {
        ON_CONNECTIVITY_CHANGED : "onConnectivityChanged"
    })

    /**
     * @ngdoc object
     * @name network.constant.NetworkStatus
     * @description
     *
     * - **`NetworkStatus.ONLINE`** - {string} - Status online
     * - **`NetworkStatus.OFFLINE`** - {string} - Status offline
     */
    .constant("NetworkStatus", {
        ONLINE : "online",
        OFFLINE: "offline"
    });
angular.module('network').service('networkInfo', NetworkInfo);

function NetworkInfo($rootScope, $q, NetworkEvents, NetworkStatus) {

    var getNetworkStatus = function() {
        return navigator.connection.type;
    };

    this.init = function(){
        document.addEventListener(NetworkStatus.ONLINE, onConnectivityChanged, false);
        document.addEventListener(NetworkStatus.OFFLINE, onConnectivityChanged, false);
    };

    function onConnectivityChanged(e) {
        $rootScope.$broadcast(NetworkEvents.ON_CONNECTIVITY_CHANGED, e);
    }

    this.isConnected = function(){
        var deferred = $q.defer();
        deferred.resolve(!navigator.connection || getNetworkStatus() != Connection.NONE);
        return deferred.promise;
    };

    this.init();
}

