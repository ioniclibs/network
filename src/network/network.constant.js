/**
 * @ngdoc overview
 * @name network.constant
 *
 * @description
 * Constantes utilizadas no módulo network.
 *
 */
angular.module("network.constant", [])

    /**
     * @ngdoc object
     * @name network.constant.NetworkEvents
     * @description
     *
     * - **`NetworkEvents.ON_CONNECTIVITY_CHANGED`** - {string} - Nome de evento para quando há alterações na conectividade.
     */
    .constant("NetworkEvents", {
        ON_CONNECTIVITY_CHANGED : "onConnectivityChanged"
    })

    /**
     * @ngdoc object
     * @name network.constant.NetworkStatus
     * @description
     *
     * - **`NetworkStatus.ONLINE`** - {string} - Status online
     * - **`NetworkStatus.OFFLINE`** - {string} - Status offline
     */
    .constant("NetworkStatus", {
        ONLINE : "online",
        OFFLINE: "offline"
    });