/**
 * @ngdoc overview
 * @name network
 *
 * @description
 * Módulo com funções utilitárias para consulta de status da internet.
 *
 * @requires network.constant
 */
angular.module('network', ['network.constant']);
