angular.module('network').service('networkInfo', NetworkInfo);

function NetworkInfo($rootScope, $q, NetworkEvents, NetworkStatus) {

    var getNetworkStatus = function() {
        return navigator.connection.type;
    };

    this.init = function(){
        document.addEventListener(NetworkStatus.ONLINE, onConnectivityChanged, false);
        document.addEventListener(NetworkStatus.OFFLINE, onConnectivityChanged, false);
    };

    function onConnectivityChanged(e) {
        $rootScope.$broadcast(NetworkEvents.ON_CONNECTIVITY_CHANGED, e);
    }

    this.isConnected = function(){
        var deferred = $q.defer();
        deferred.resolve(!navigator.connection || getNetworkStatus() != Connection.NONE);
        return deferred.promise;
    };

    this.init();
}

